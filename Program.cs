﻿using System;
using System.Collections.Generic;

namespace factoryPatternimplementation
{
    class Program
    {
        static void Main(string[] args)
        {
            objectFactory Objectfactory = new objectFactory();
            Dictionary<string, object> data= new Dictionary<string, object>();
            data.Add("ID", 1);
            gmail gmail = Objectfactory.Get(ObjectType.gmail, data) as gmail;
            hotmail hotmail = Objectfactory.Get(ObjectType.hotmail, data) as hotmail;

            Console.WriteLine(gmail.getInfo());
            Console.WriteLine(hotmail.getInfo());
            Console.ReadLine();
        }
    }

    enum ObjectType
    {
        gmail,
        hotmail
    }



    //class Fcatory

        class objectFactory
        {

        private ObjectType _type;
        private Dictionary<string, object> _data;
        public IObject Get(ObjectType type, Dictionary<string, object> data)
        {
            _type = type;
            _data = data;
            return GetObject();
        }


        private IObject GetObject()
        {
            IObject obj = null;



            Int32 id = Convert.ToInt32(_data["ID"]);

            switch(_type)
            {
                case ObjectType.gmail:
                    if (id == 1)
                    {
                        obj = new gmail()
                        {
                            emailId = "who@gmail.com",
                            name = "who",
                            phoneNo = 1234569
                        };
                    }
                    break;


                case ObjectType.hotmail:
                    if (id == 1)
                    {
                        obj = new hotmail()
                        {
                            emailId = "who@hotmail.com",
                            name = "who",
                            phoneNo = 1234569
                        };
                    }

                    break;

                default:
                    obj = null;
                    break;
            }


            return obj;
        }


       }

    interface IObject
    {
        string getInfo();
    }



    class gmail : IObject
    {
        public string emailId;
        public string name;
        public Int32 phoneNo;


        public virtual string getInfo()
        {
            return string.Format("Emailid is {0}.Name of sender is {1}. Phone number of the sender is {2}.", emailId, name, phoneNo);
        }

/*


        class Program
        {
            static void Main(string[] args)
            {
                try
                {
                    SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                    client.EnableSsl = true;
                    client.Timeout = 100000;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.UseDefaultCredentials = false;
                    client.Credentials = new NetworkCredential("mailnow4akash@gmail.com", "Karan12345@");
                    MailMessage message = new MailMessage();
                    message.To.Add("karan1198kumar@gmail.com");
                    message.From = new MailAddress("mailnow4akash@gmail.com");
                    message.Subject = "mail check";
                    message.Body = "First mail to check the process";
                    client.Send(message);


                    Console.WriteLine("email was sent successfully!");
                }
                catch (Exception ep)
                {
                    Console.WriteLine("failed to send email with the following error:");
                    Console.WriteLine(ep.Message);
                }
            }
        }



    */

    }


    class hotmail : IObject
    {
        public string emailId;
        public string name;
        public Int32 phoneNo;

        public virtual string getInfo()
        {
            return string.Format("Emailid is {0}.Name of sender is {1}. Phone number of the sender is {2}.", emailId, name, phoneNo);
        }


    }
}
